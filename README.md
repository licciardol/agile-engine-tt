# Agile Engine
## Get Started
1. **Install [Node](https://nodejs.org)**.
2. **Clone this repository.** - `git clone https://bitbucket.org/licciardol/agile.git` 
4. **Make sure you're in the directory you just created.** 
5. **Follow backend API** 
6. **Follow frontend API** 

# Agile Engine backend API

## Get Started
1. **Make sure you're in the backend directory** - `root/backend`
2. **Install Node Packages.** - `npm install`
3. **Run the app.** - `npm run start`

## Endpoints
1. - `localhost:4000/api/transactions` **GET** **POST**
2. - `localhost:4000/api/transactions/:transactionId` **GET**
3. - `localhost:4000/api/account` **GET**

**NOTE: running on port 4000 by default**
**NOTE config is set by default on dev**

# Agile Engine fronted API
## Get Started
1. **Make sure you're in the frontend directory** - `root/frontend`
2. **Install Node Packages.** - `npm install`
3. **Run the app in dev** - `npm run start`

**NOTE: running on port 4200 by default**
**NOTE To run in production use** `npm start -- --prod`
