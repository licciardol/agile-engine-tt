class NotFoundError extends Error {
    constructor(message) {
      super(message);
      this.name = "NotFoundError";
    }
}

class InvalidFormatError extends Error {
    constructor(message) {
      super(message);
      this.name = "InvalidFormatError";
    }
}

class InsufficientFundsError extends Error {
    constructor(message) {
      super(message);
      this.name = "InsufficientFundsError";
    }
}

module.exports = {NotFoundError, InvalidFormatError, InsufficientFundsError}
  