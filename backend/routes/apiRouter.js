const express = require('express');
const apiController = require('../controllers/apiController');

function routes() {
  const apiRouter = express.Router();
  const controller = apiController();
  apiRouter.route('/transactions')
    .post(controller.newTransaction)
    .get(controller.getTransactions);
  apiRouter.route('/transactions/:transactionId')
    .get(controller.getTransactionById)
  apiRouter.route('/account')
    .get(controller.getAccount)

  return apiRouter;
}

module.exports = routes;