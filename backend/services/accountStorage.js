class AccountStorage {
  constructor(){ 
    if (AccountStorage.instance)
      return AccountStorage.instance
    AccountStorage.instance = this

    this.amount = 0
  }

  static instance
}

module.exports = {AccountStorage}