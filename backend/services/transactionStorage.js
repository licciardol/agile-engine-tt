// import {ITransaction} from '../models/transaction.interface'
const {NotFoundError, InvalidFormatError, InsufficientFundsError} = require('../errors') 
const { v1: uuidv1 } = require('uuid');

class TransactionStorage {
  constructor(){
    if (TransactionStorage.instance)
      return TransactionStorage.instance
    TransactionStorage.instance = this

    this.transactionList = []
    this.transactionIds = {}
  }
  
  static instance

  listTransactions() {
    return this.transactionList
  }

  getTransactionById(id){
   if(!(id in this.transactionIds)) throw new NotFoundError(id)
   return this.transactionList[this.transactionIds[id]]
  }

  newTransaction(transaction){
    transaction.id  = uuidv1();
    transaction.effectiveDate = new Date().toISOString();
    this.transactionList.push(transaction);
    this.transactionIds[transaction.id] = this.transactionList.length - 1

    return transaction
  }
}

module.exports = {TransactionStorage}