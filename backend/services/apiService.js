const {TransactionStorage} =require('./transactionStorage')
const {AccountStorage}=require('./accountStorage')
const {InsufficientFundsError} = require('../errors') 

const {RWLock} = require('async-rwlock');
 
class ApiService {
  constructor(){
    if (ApiService.instance)
      return ApiService.instance
    ApiService.instance = this
    // this.rwLock = new Lock()
    this.transactionStorage = new TransactionStorage()
    this.accountStorage = new AccountStorage()
    this.lock = new RWLock();

  }  

  async newTransaction(transaction) {
    await this.lock.writeLock();
    let current = transaction.type === 'credit'? this.accountStorage.amount + transaction.amount : this.accountStorage.amount - transaction.amount 

    if(current >= 0) {
      this.accountStorage.amount = current
      const nTransaction = this.transactionStorage.newTransaction(transaction)
      this.lock.unlock();
      return nTransaction
    }
    this.lock.unlock();
    throw new InsufficientFundsError('')
  }

  async getTransactions() {
    await this.lock.readLock();
    const list = this.transactionStorage.listTransactions();
    this.lock.unlock();
    return list
  }

  async getTransactionById(id) {
    await this.lock.readLock();
    try {
      return this.transactionStorage.getTransactionById(id)
    } catch(e){
      throw e
    } finally {
      this.lock.unlock();
    }
  }

  getAccount() {
    return this.accountStorage.amount
  }
}

module.exports = {ApiService}