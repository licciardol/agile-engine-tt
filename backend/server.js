const express = require('express');

const bodyParser = require('body-parser');
const config = require('./config'); // get our config file

const app = express();
// // =======================
// // configuration =========
// // =======================
const port = config.app.port;

const apiRouter = require('./routes/apiRouter')(); // get our routes file

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

//  middleware to enable CORS
app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header('Access-Control-Allow-Methods', 'GET,PUT,PATCH,POST,DELETE');
  res.header("Access-Control-Allow-Headers", "Origin, Content-Type, Accept");
  // intercept OPTIONS method
  if ('OPTIONS' == req.method) {
    res.send(200);
  }
  else {
    next();
  }
});

app.use('/api', apiRouter);

app.get('/', (req, res) => {
  res.send('Welcome to my Nodemon API!');
});

app.server = app.listen(port, () => {
  console.log(`Running on port ${port}`);
});

module.exports = app;

