# Agile Engine backend API

## Get Started
1. **Make sure you're in the backend directory** - `root/backend`
2. **Install Node Packages.** - `npm install`
3. **Run the app.** - `npm run start`

## Endpoints
1. - `localhost:4000/api/transactions` **GET** **POST**
2. - `localhost:4000/api/transactions/:transactionId` **GET**
3. - `localhost:4000/api/account` **GET**

**NOTE: running on port 4000 by default**
**NOTE config is set by default on dev**