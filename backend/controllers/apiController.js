const {NotFoundError, InvalidFormatError, InsufficientFundsError} = require('../errors') 
const {ApiService} = require('../services/apiService.js')
const {Transaction} = require('../models/transaction')


const {isUUID} = require('../validator')
const apiService = new ApiService()
function apiController() {
  
  async function newTransaction(req, res) {
    const {type, amount} = req.body
    const transaction = new Transaction(type, amount);
    const response = {};
    try {
      response.data = await apiService.newTransaction(transaction)
      res.status(201);
      return res.json(response);
    } catch(e) {
      if (e instanceof InsufficientFundsError) {
        res.status(418);
        return res.send('Insufficient Funds')
      }
      res.status(500);
      return res.send('Internal Server Error')
    }
  }
  async function getTransactions(req, res) {
    const response = {}
    try {
      response.data = await apiService.getTransactions()
      res.status(200);
      return res.json(response);
    } catch(e) {
      res.status(500);
      return res.send('Internal Server Error')
    }
  }

  async function getTransactionById(req, res) {
    
    const response = {}
    const id = req.params.transactionId;
    if(!isUUID(id)){
      res.status(400);
      return res.send('Invalid format')
    }
    try {
      response.data = await apiService.getTransactionById(id)
      res.status(200);
      return res.json(response);
    } catch(e) {
      if (e instanceof NotFoundError) {
        res.status(404);
        return res.send('Not Found')
      }
      res.status(500);
      return res.send('Internal Server Error')
    }
  }

  function getAccount(req, res) {
    const response = {};
    try {
      response.data = apiService.getAccount()
      res.status(200);
      return res.json(response);
    } catch(e) {
      res.status(500);
      return res.send('Internal Server Error')
    }
  }
  return { newTransaction, getTransactions, getTransactionById, getAccount };
}



module.exports = apiController;
