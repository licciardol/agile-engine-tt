// config.js
const env = process.env.NODE_ENV || 'dev';

const dev = {
  app: {
    port: parseInt(process.env.DEV_APP_PORT) || 4000
  }
};
const test = {
  app: {
    port: parseInt(process.env.TEST_APP_PORT) || 4000
  }
};

const config = {
  dev,
  test
};

module.exports = config[env];