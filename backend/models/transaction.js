

class Transaction { 
  constructor(type, amount) {      
      this.type = type;
      this.amount = amount;
      this.id = undefined;
      this.effectiveDate = undefined;
  }
}

module.exports = {Transaction}
