import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms'
import { AppComponent } from './app.component';
import { TransactionListComponent } from './transaction/transaction-list.component';
import { NavBarComponent } from './nav/nav-bar.component';
import { TransactionService } from './transaction/shared/transaction.service';
import { TransactionListResolver } from './transaction/transaction-list-resolver.service';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [
    AppComponent,
    NavBarComponent,
    TransactionListComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule, 
    ReactiveFormsModule
  ],
  providers: [
    TransactionService,
    TransactionListResolver
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
