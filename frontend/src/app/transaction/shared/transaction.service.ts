import { Injectable } from '@angular/core'
import { Observable, of } from 'rxjs'
import { HttpClient } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { environment } from '../../../environments/environment';

@Injectable()
export class TransactionService {
  constructor(private http: HttpClient) {

  }
  private API_URL = environment.API_URL
  getTransactions() {
    return this.http.get(`${this.API_URL}/api/transactions`)
      .pipe(catchError(this.handleError('getTransactions', [])))
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error);
      return of(result as T);
    }
  }
}
