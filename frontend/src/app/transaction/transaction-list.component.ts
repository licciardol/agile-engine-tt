import { Component, OnInit } from '@angular/core'
import { TransactionService } from './shared/transaction.service'
import { ITransaction } from '../models/transaction.interface'

import { ActivatedRoute } from '@angular/router'

@Component({
  selector: 'transaction-list',
  templateUrl: './transaction-list.component.html'
})
export class TransactionListComponent implements OnInit {
  transactions: ITransaction[]

  constructor(private hotelService: TransactionService, private route: ActivatedRoute) {

  }

  ngOnInit() {
    this.transactions = this.route.snapshot.data['transactions']['data'].map(el => {
      el.hidden=true;
      return el;
    })
    
    console.log(this.transactions)
  }
}

