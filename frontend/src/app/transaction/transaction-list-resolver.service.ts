import { Injectable } from '@angular/core'
import { Resolve } from '@angular/router'
import { TransactionService } from './shared/transaction.service'

@Injectable()
export class TransactionListResolver implements Resolve<any> {
  constructor(private transactionService:TransactionService) {

  }

  resolve() {
    return this.transactionService.getTransactions()
  }
}