export interface ITransaction {
  id:string,
  type: transactionType,
  amount: number,
  effectiveDate: string
}

enum transactionType {
  Credit = 'credit',
  Debit = 'debit'
}