import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TransactionListComponent } from './transaction/transaction-list.component';
import { TransactionListResolver } from './transaction/transaction-list-resolver.service';


const routes: Routes = [
  {path: 'transactions', component: TransactionListComponent, resolve: {transactions:TransactionListResolver}},
  {path: '', redirectTo: '/transactions', pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
